/* VARIABLES */
// Capturamos la variable "lado" por URL
let queryString = window.location.search;
let urlParams = new URLSearchParams(queryString);
let lado = urlParams.get('lado');

// Constante que indica el tamaño del tablero (ancho*alto)
// Y a la que le daremos un valor a traves de la función crearTablero
const TAMANO_TABLERO = crearTablero(lado);

// Inicializamos las variables que utilizaremos más adelante
let numBarcos = 0;
let numHits = 0; 

// Llamada al metodo inicializar para dar un valor distinto de 0 a las variables numBarcos y numHits
inicializar();

// Creación de los dos jugadores
let jugador1 = new Array(TAMANO_TABLERO + 1);
let jugador2 = new Array(TAMANO_TABLERO + 1);

// Variable que tendrá el valor 1 o 2
// Indicará quién tiene el turno activo
let turnoJugador = null;
// Contador de turnos
let turno = 0;

// Resto de variables
let barcoPosicionado = null;
let tipoBarco = null;
let jugador = null;

// Creamos los arrays correspondientes a los jugadores 
// que será un array contenedor de las casillas correspondientes a su tablero
$(function () {
  // Añadimos el valor para el indice del primer array
  for (let i = 0; i < jugador1.length; i++) {
    jugador1[i] = new Array(TAMANO_TABLERO + 1);
    jugador2[i] = new Array(TAMANO_TABLERO + 1);
  }
  // Añadimos el valor para el indice del segundo array
  for (let i = 0; i < jugador1.length; i++) {
    for (let j = 0; j < jugador1.length; j++) {
      // Inicializamos las casillas a false
      jugador1[i][j] = false;
      jugador2[i][j] = false;
    }
  }

  /**
   * CREACIÓN DE LOS TABLEROS
   */

  // Capturamos los dos tableros
  let tablero1 = document.getElementById("jugador1-tablero");
  let tablero2 = document.getElementById("jugador2-tablero");
  imprimirTableros(tablero1, tablero2);

  /**
   * COGER UN BARCO PARA POSICIONARLO
   */

  // Damos a todos los .barco la siguiente función
  $(".barco").click(colocarBarco);

  /**
   * POSICIONAR UN BARCO
   */

  // Al elemento body le pasamos la función click y el parametro evento
  $("body").on("click", ".place-barco", function (evento) {
    if (comprobarCasilla(jugador, barcoPosicionado, tipoBarco, evento.target)) {
      let parent = jugador == 1 ? "#izquierda" : "#derecha";
      // Remueve un barco de la zona clickeada
      $(parent + " .barco-placeholder > ." + tipoBarco).css("display", "none");
      // Remueve las clases de los cuadrados vacios
      $(".board > div").not(".barco").removeClass();
      let linea = evento.target.id.substr(0, 1);
      let columna = evento.target.id.substr(1, 1);
      let longitudBarco = tipoBarco.substr(tipoBarco.length - 1, 1);

      // Si el barco posicionado tiene la clase vertical...
      // Le añadimos la clase barco+tipoBarco+vertical al div con el id con la clave lineacolumna-jugador
      if ($(barcoPosicionado).hasClass("vertical")) {
        $("#" + linea + columna + "-" + jugador).addClass(
          "barco " + tipoBarco + " vertical"
        );
        // Pinto anterior y siguiente
        $(`#${Number(linea)-1}${Number(columna)}-${jugador}`).addClass("ocupado");
        $(`#${Number(linea)+Number(longitudBarco)}${Number(columna)}-${jugador}`).addClass("ocupado");

        // Y sus adyacentes
        $(`#${Number(linea)}${Number(columna)+1}-${jugador}`).addClass("ocupado");
        $(`#${Number(linea)}${Number(columna)-1}-${jugador}`).addClass("ocupado");
        $(`#${Number(linea)-1}${Number(columna)-1}-${jugador}`).addClass("ocupado");
        $(`#${Number(linea)-1}${Number(columna)+1}-${jugador}`).addClass("ocupado");
        $(`#${Number(linea)+Number(longitudBarco)}${Number(columna)+1}-${jugador}`).addClass("ocupado");
        $(`#${Number(linea)+Number(longitudBarco)}${Number(columna)-1}-${jugador}`).addClass("ocupado");

        for (let i = 1; i < longitudBarco; i++) {
          $("#" + (parseInt(linea) + i) + columna + "-" + jugador).addClass("barco");
          // Pinto adyacentes a la linea
          $(`#${Number(linea)+i}${Number(columna)+1}-${jugador}`).addClass("ocupado");
          $(`#${Number(linea)+i}${Number(columna)-1}-${jugador}`).addClass("ocupado");
        }

        // Si el barco tiene la clase horizontal...
      } else {
        $("#" + linea + columna + "-" + jugador).addClass("barco " + tipoBarco);

        // Pinto anterior y siguiente
        $(`#${Number(linea)}${Number(columna)-1}-${jugador}`).addClass("ocupado");
        $(`#${Number(linea)}${Number(columna)+Number(longitudBarco)}-${jugador}`).addClass("ocupado");
        
        // Y sus adyacentes
        $(`#${Number(linea)-1}${Number(columna)-1}-${jugador}`).addClass("ocupado");
        $(`#${Number(linea)+1}${Number(columna)-1}-${jugador}`).addClass("ocupado");
        $(`#${Number(linea)+1}${Number(columna)}-${jugador}`).addClass("ocupado");
        $(`#${Number(linea)-1}${Number(columna)}-${jugador}`).addClass("ocupado");
        $(`#${Number(linea)+1}${Number(columna)+Number(longitudBarco)}-${jugador}`).addClass("ocupado");
        $(`#${Number(linea)-1}${Number(columna)+Number(longitudBarco)}-${jugador}`).addClass("ocupado");

        for (let i = 1; i < longitudBarco; i++) {
          $("#" + linea + (parseInt(columna) + i) + "-" + jugador).addClass("barco");
          // Pinto adyacentes a la linea
          $(`#${Number(linea)+1}${Number(columna)+i}-${jugador}`).addClass(" ocupado");
          $(`#${Number(linea)-1}${Number(columna)+i}-${jugador}`).addClass(" ocupado");
        }
      }


      // Si el jugador es el 1... ocultamos el barcos posicionado del jugador 1 (izq)
      if (jugador === 1) {
        if ($('#izquierda .barco[style="display: none;"]').length === numBarcos) {
          $("button#" + jugador).removeClass("hidden");
        }
        // Lo mismo pero para el jugador 2 (der)
      } else {
        if ($('#derecha .barco[style="display: none;"]').length === numBarcos) {
          $("button#" + jugador).removeClass("hidden");
        }
      }
    }
  });

  /**
   * ESCONDEMOS LOS BARCOS PARA EMPEZAR EL JUEGO
   */

  // A los botones les damos la siguiente funcion
  $("button").click(function (evento) {
    // Guardamos los barcos con la llamada la funcion guardarBarcos
    guardarBarcos(evento.target.id);
    // Cogemos el boton y lo escondemos
    $(evento.target).hide();
    // Si el boton.length == 2 (Lo ha pulsado tanto el jugador 1 y 2)
    if ($("button[style]").length == 2) {
      // Le damos la clase shoot
      $("#jugador1-tablero").addClass("shoot");
      // Mostramos los marcadores
      $(".puntuacion").show();
      // Inicializamos el contador de turno
      turnoJugador = 1;
    }
  });

  /**
   * RECARGAMOS LA PAGINA
   */

  // Le damos al boton con el id reset la siguiente función
  $("#reset").click(reset);

  /**
   * DISPARAR
   */

  // Le damos la siguiente funcion al tablero con la clase shoot
  $("body").on("click", ".shoot > div", function (evento) {
    // Capturamos la linea y la columna
    let linea = evento.target.id.substr(0, 1);
    let columna = evento.target.id.substr(1, 1);

    // Si la casilla no tiene la clase bomba...
    if (!$(evento.target).hasClass("bomba")) {
      // Si el turno del jugador es el 1 (jugador izquierda)
      if (turnoJugador === 1) {
        // Comprobamos si la posicion del array con la clave linea-columna es true
        // Si true, le añadimos a esa casilla la clase barci bomba (y sumamos 1 a los impactos y disparos)
        if (jugador2[linea][columna]) {
          $(evento.target).addClass("barco bomba");
          $("#izquierda #disparos").html("Disparos: " + $("#izquierda .bomba").length);
          $("#izquierda #impactos").html(
            "Impactos: " + $("#izquierda .barco.bomba").length
          );
          // Si false, le añadimos a esa casilla la clase bomba (y sumamos 1 a los disparos)
        } else {
          $(evento.target).addClass("bomba");
          $("#izquierda #disparos").html("Disparos: " + $("#izquierda .bomba").length);
        }
        // Si el turno del jugador es el 2 (jugador der)
      } else {
        if (jugador1[linea][columna]) {
          $(evento.target).addClass("barco bomba");
          $("#derecha #disparos").html("Disparos: " + $("#derecha .bomba").length);
          $("#derecha #impactos").html(
            "Impactos: " + $("#derecha .barco.bomba").length
          );
        } else {
          $(evento.target).addClass("bomba");
          $("#derecha #disparos").html("Disparos: " + $("#derecha .bomba").length);
        }
      }
      
      // Si el numero de impactos es numHits...
      if ($("#jugador" + turnoJugador + "-tablero .barco.bomba").length === numHits) {
        // El jugador "turnoJugador" es el ganador
        $("#p" + turnoJugador)
          .html("Winner")
          .css("columnaor", "#AFB42B");
        $(".shoot").addClass("winner");
        // Removemos la clase disparo
        $(".shoot").removeClass("shoot");
        // Añadimos al fondo del ganador una copa
        $('.winner').css('background-image', 'url(https://guestoria.com/wp-content/uploads/2019/04/winner-gif-1.gif)');
        // Removemos los puntos (bombas) para que la copa se vea correctamente
        $('.winner').children().css('border', '0px solid black').removeClass('bomba');
        // Refrescamos la pagina para volver a empezar
        setTimeout(() => {
          reset();
        }, 5000);
        // Si todavia no hay ganador le quitamos el turno al jugador activo 
      } else {
        // Removemos la clase disparar al jugador activo
        $("#jugador" + turnoJugador + "-tablero").removeClass("shoot");
        // Pasamos el turno al otro jugador
        turnoJugador = (++turno % 2) + 1;
        // Le damos la clase disparar al jugador con el nuevo turno
        $("#jugador" + turnoJugador + "-tablero").addClass("shoot");
      }
    }
  });
});

/**
 * RECOGEMOS EL VALOR DADO EN EL FORMULARIO PARA CREAR DINÁMICAMENTE EL TABLERO DE JUEGO
 * @param lado valor del input[type=number] del formulario inicial previo al juego
 * @returns lado
 */
function crearTablero(lado) {
  switch (true) {
    case (lado < 4):
      lado = 4;
      break;
    case (lado > 10):
      lado = 10;
      break;
    default:
      lado = lado;
      break;
  }
  return Number(lado);
}

/**
 * METODO QUE GESTIONA LAS SIGUIENTES VARIABLES EN FUNCIÓN DE LA VARIABLE LADO:
 * numBarcos (número total de barcos, contando ambos jugadores) 
 * numHits (pool de impactos del total de los barcos de cada jugador)
 */
function inicializar() {
  if (lado < 6) {
    // No creamos barcos
    // Por defecto 2 barcos para cada jugador
    numBarcos = 4;
    numHits = 5;

  } else if (lado >= 6 && lado <= 8) {
    numBarcos = 6;
    numHits = 8;
    // Creamos hasta 3 barcos
    $(".containerBarcosH").append('<div class="barco-placeholder"><div class="barco barco3v3"></div></div>');
    $(".containerBarcosV").append('<div class="barco-placeholder vertical"><div class="barco barco3v3 vertical"></div></div>');

  } else {
    numBarcos = 10;
    numHits = 17;
    // Creamos hasta 5 barcos
    $(".containerBarcosH").append('<div class="barco-placeholder"><div class="barco barco3v3"></div></div>');
    $(".containerBarcosH").append('<div class="barco-placeholder"><div class="barco barco4"></div></div>');
    $(".containerBarcosH").append('<div class="barco-placeholder"><div class="barco barco5"></div></div>');
    $(".containerBarcosV").append('<div class="barco-placeholder vertical"><div class="barco barco3v3 vertical"></div></div>');
    $(".containerBarcosV").append('<div class="barco-placeholder vertical"><div class="barco barco4 vertical"></div></div>');
    $(".containerBarcosV").append('<div class="barco-placeholder vertical"><div class="barco barco5 vertical"></div></div>');
  }
}

/**
 * PASAMOS COMO PARAMETROS LA POSICIÓN DE LOS DOS TABLEROS PARA CREARLOS
 * CON UN BUCLE ANIDADO CREAMOS LOS TABLEROS CASILLA A CASILLA
 * @param tablero1 posicion del tablero1 para rellenarlo con los divs
 * @param tablero2 posicion del tablero2 para rellenarlo con los divs
 */
function imprimirTableros(tablero1, tablero2) {
  // For anidado para la creación de los dos tableros
  for (let i = 1; i <= TAMANO_TABLERO; i++) {
    // For de filas
    for (let j = 1; j <= TAMANO_TABLERO; j++) {
      // For de columnas
      // Creamos un div para el primer tablero
      let div1 = document.createElement("div");
      div1.id = i + "" + j + "-1";
      div1.style.width = 100 / TAMANO_TABLERO + "%";
      div1.style.height = 100 / TAMANO_TABLERO + "%";
      tablero1.appendChild(div1);

      // Creamos un div para el segundo tablero
      let div2 = document.createElement("div");
      div2.id = i + "" + j + "-2";
      div2.style.width = 100 / TAMANO_TABLERO + "%";
      div2.style.height = 100 / TAMANO_TABLERO + "%";
      tablero2.appendChild(div2);
    }
  }
}

/**
 * DETECTA EL BARCO EN EL QUE HAS HECHO CLICK PARA POSICIONARLO
 * @param evento el barco para posicionar
 */
function colocarBarco(evento) {
  // Capturamos el barco seleccionado con evento.target
  barcoPosicionado = evento.target;
  // Capturamos la clase del evento.target
  // Puesto que tiene 2 clases nos quedamos con la segunda
  // Utilizando el split [barco, barco2] --> barco2 porque capturas la posicion 1
  tipoBarco = barcoPosicionado.className.split(" ")[1];
  
  // Si el barco seleccionado es hijo del id "izquierda" pertenece a jugador 1
  if ($(evento.target).parents("#izquierda").length) jugador = 1;
  // Sino pertenece al jugador 2
  else jugador = 2;
  
  // Cambiamos la clase del barco (removemos la clase barco y le añadimos place-barco y si tenia la clase vertical, se la añadimos)
  $(".board > div").not(".barco").not(".ocupado").removeClass();
  $(".board > div")
    .not(".barco")
    .addClass("place-barco " + tipoBarco);
  if ($(barcoPosicionado).hasClass("vertical"))
    $(".board > div").not(".barco").addClass("vertical");

};

/**
 * CHEQUEA QUE UN BARCO ESTE BIEN POSICIONADO. 
 * EJEMPLO: QUE NO ESTE FUERA DEL TABLERO, QUE NO HAYA OTRO BARCO ENCIMA...
 * @param  jugador    int (el jugador que posiciona el barco, 1 = izquierda, 2 = derecha)
 * @param  barcoPosicionado string (div del barco que será posicionado en el tablero)
 * @param  tipoBarco  string (clase con el nombre del tipo de barco; Ejemplo: barco2)
 * @param  box       string (div del tablero que hemos seleccionado para posicionar el barco) De la clase de este parametro sacarémos la línea y columna
 * @return          devuelve true si el barco esta bien posicionado, false si el barco esta mal posicionado;
 * Solo posiciona el barco si la función devuelve true
 */
function comprobarCasilla(jugador, barcoPosicionado, tipoBarco, box) {
  debugger
  let longitudBarco = tipoBarco.substr(tipoBarco.length - 1, 1);
  let linea = box.id.substr(0, 1);
  let columna = box.id.substr(1, 1);
  let valido = true;

  if ($("#" + linea + columna + "-" + jugador).hasClass("ocupado")) {
    valido = false;
  }
  if ($(barcoPosicionado).hasClass("vertical")) {
    // Barco vertical
    // Si el tamaño del tablero - longitud del barco es menor que el de la linea-1
    // Quiere decir que el barco esta posicionado fuera del tablero, devuelve false
    if (TAMANO_TABLERO - longitudBarco < linea - 1) return false;
    // Comprobamos que las casillas donde se posicionaría el barco no están ocupadas
    for (let i = 0; i < longitudBarco; i++) {
      if ($("#" + (parseInt(linea) + i) + columna + "-" + jugador).hasClass("barco")) {
        valido = false;
      }

    }
  } else {
    // Barco horizontal
    if (TAMANO_TABLERO - longitudBarco < columna - 1) return false;
    for (let i = 0; i < longitudBarco; i++) {
      if ($("#" + linea + (parseInt(columna) + i) + "-" + jugador).hasClass("barco")) {
        valido = false;
      }
    }
  }
  // Devolvemos siempre true salvo que el metodo en los diferentes puntos de control haya devuelto false
  return valido;
}

/**
 * Guardamos los barcos en una matriz y los ocultamos del tablero
 * @param  jugador  int 1 (jugador izq) o 2 (jugador der)
 */
function guardarBarcos(jugador) {
  // For anidado donde recorremos las casillas para comprobar...
  // Si el id con la clave filaColumna+jugador tiene la clase barco
  // La casilla #ij pasa a true
  for (let i = 1; i <= TAMANO_TABLERO; i++) {
    for (let j = 1; j <= TAMANO_TABLERO; j++) {
      debugger
      if ($("#" + i + j + "-" + jugador).hasClass("barco")) {
        if (jugador == 1) jugador1[i][j] = true;
        else jugador2[i][j] = true;
      }
    }
  }
  // Removemos las clases correspondientes de los tableros
  $("#jugador" + jugador + "-tablero > div").removeClass();
  if (jugador == 1) $("#izquierda .barco").removeClass();
  else $("#derecha .barco").removeClass();
}

/*
 * FUNCIÓN PARA RECARGAR LA PARTIDA 
 * O BIEN CUANDO PULSAMOS EL BOTÓN DEL RESET O BIEN CUANDO HAY UN GANADOR
 */
function reset() {
  location.href = "./form.html";
}