/* sum.js */

module.exports = {

  crearTablero: function (lado) {
    switch (true) {
      case lado < 4:
        lado = 4;
        break;
      case lado > 10:
        lado = 10;
        break;
      default:
        lado = lado;
        break;
    }
    return Number(lado);
  },
  
  comprobarCasilla: function (jugador, barcoPosicionado, tipoBarco, box) {
    
    let longitudBarco = tipoBarco;
    //let longitudBarco=tipoBarco;
    let linea = box.charAt(0);
    let columna = box.charAt(1);

      if (10 - longitudBarco < linea - 1) return false; // 10 --> TAMANO_TABLERO
      // Si lo vamos a posicionar dentro del tablero, comprobamos que esas casillas
      // No estan ocupadas ya por otro barco
      for (let i = 0; i < longitudBarco; i++) {
        // Si la casilla ya esta ocupada, devolvemos false
        if (
          $("#" + (parseInt(linea) + i) + columna + "-" + jugador).hasClass(
            "barco"
          )
        )
          return false;
      }
    
      // Barco horizontal
      if (10 - longitudBarco < columna - 1) return false;
      for (let i = 0; i < longitudBarco; i++) {
        if (
          $("#" + linea + (parseInt(columna) + i) + "-" + jugador).hasClass(
            "barco"
          )
        )
          return false;
      }
    
    // Devolvemos siempre true salvo que el metodo en los diferentes puntos de control haya devuelto false
    return true;
  },
};

