let sum = require("./main.js");
let assert = require("chai").assert;

const crearTableroTest = sum.crearTablero(5);
const crearTableroMenorTest = sum.crearTablero(3);
const crearTableroMayorTest = sum.crearTablero(12);



describe("#crearTablero", function () {
  context("without arguments", function () {
    it("should return 5", function () {
      assert.equal(crearTableroTest, 5);
    });
    it("should return 4", function () {
      assert.equal(crearTableroMenorTest, 4);
    });
    it("should return 10", function () {
      assert.equal(crearTableroMayorTest, 10);
    });
  });
});
